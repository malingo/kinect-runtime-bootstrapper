Kinect Runtime Chain Installation

The files in the 'KinectRuntime1_8' directory are used to embed
the Kinect Runtime installer as a prerequisite for a ClickOnce
installer for any Kinect application.  

To set up the prerequisite, copy the runtime installer executable
from the following path on a development machine with the Kinect
SDK installed...

  C:\Program Files\Microsoft SDKs\Kinect\v1.8\Redist

...to the 'KinectRuntime1_8' directory (same level as the
product.xml file).  

NOTE: do not check in the runtime-installer executable to git.

The whole 'KinectRuntime1_8' directory should then be copied
to the following package bootstrapper location so that it can
be picked up by Visual Studio as a legit prerequisite:

  C:\Program Files (x86)\Microsoft SDKs\Windows\v8.1A\Bootstrapper\Packages

Remove any earlier versions of the runtime package from the
aforementioned directory, and restart VS if the Kinect runtime
doesn't show up in the prereq list.

In the clickonce setup for any Kinect app, select the runtime
installer as a prereq, and it will be installed (if necessary) on
the target machine.

** IMPORTANT NOTE: it seems as though when a new version of this
runtime installer is set as a prerequisite, the end user must run
the setup.exe file from the download location, since the normal
application upgrade will not cause the new prereq bootstrapper
to run.

